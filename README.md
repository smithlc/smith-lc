SMITH LC is a full-service civil litigation and transactional law firm focused on business transactions, business litigation, investor fraud, real estate, construction, estate planning, probate, product/premises liability, intellectual property, labor and employment, bankruptcy and appellate law.

Address: 3161 Michelson Dr, Suite 925, Irvine, CA 92612, USA

Phone: 949-416-5000